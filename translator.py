import wordgen as wg

with open("dict.txt") as f:
	content = f.readlines()

for i in range(len(content)):
	content[i] = content[i].rstrip()

language = []

for i in range(len(content)):
    successful = False
    while not successful:
        word = wg.wordmaker([1,2,3,4], 2)
        if word not in language:
            language.append(word)
            successful = True

for entry in zip(content, language):
	print(entry)

