import random
import sys

consonants = ["p", "t", "k", "p'", "t'", "k'", "b", "d", "g", "h", "s", "l", "y", "x", "w", "ts", "m", "n", "r", "f", "v", "sh", "ng", "ts'"]
vowels = ['a', 'i', 'e', 'o', 'u']

NUM_SYLS = 3
EXPLICIT = False
if len(sys.argv) >= 2:
	NUM_SYLS = int(sys.argv[1])
	EXPLICIT = True

SYL_TYPES = [1, 2, 3, 4]
# Syllable types = 	1 => CVC
#					2 => CV
#					3 => VC
#					4 => V

def gen_template(allowedtypes):
	return [allowedtypes[random.randint(0,len(allowedtypes)-1)], consonants[random.randint(0,len(consonants)-1)], vowels[random.randint(0,len(vowels)-1)], consonants[random.randint(0,len(consonants)-1)]]
# Template
# t = 	
#		0. type 
#		1. C 
#		2. V 
#		3. C

def template_syl(template):
	if template[0] == 1:
		# CVC
		return template[1] + template[2] + template[3]
	elif template[0] == 2:
		#CV
		return template[1] + template[2]
	elif template[0] == 3:
		#VC
		return template[2] + template[3]
	elif template[0] == 4:
		#V
		return template[2]

def wordmaker(allowedtypes, num_syls):
	word = ''
	syls = random.randint(1, num_syls)
	if EXPLICIT:
		syls = num_syls
	for syl in range(0, syls):
		word += template_syl(gen_template(allowedtypes))
	return word

print(wordmaker(SYL_TYPES, NUM_SYLS))
